package com.hp.tools;

import org.junit.Test;

import java.io.File;
import java.net.URL;

public class TemplateLoadTest {

    @Test
    public void load() {
        ClassLoader classLoader = getClass().getClassLoader();
        URL url1 = classLoader.getResource("templates/tables.ftl");
        URL url2 = TemplateLoadTest.class.getClassLoader().getResource("jdbc.properties");

        System.out.println(new File(url1.getPath()));
        System.out.println(new File(url2.getPath()));

        System.out.println(new File(url1.getPath()).getParentFile());
        System.out.println(new File(url2.getPath()).getParentFile());
    }
}
