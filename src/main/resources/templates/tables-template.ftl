<html lang="en">
<head>
    <title>${database}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <style>
        body {
            font-family: "微软雅黑", Verdana, Arial, Helvetica, sans-serif;
            margin: 0;
            padding: 0;
        }

        .normal {
            font-size: 11px;
            font-weight: normal;
            color: #000000;
            overflow-wrap: break-word;
        }

        .fieldcolumn {
            color: #000000;
            font-size: 10px;
            font-weight: 600;
            background-color: #ffffff;
        }

        .c1 {
            width: 25%;
            background: #dbdee2;
        }

        .k1 {
            width: 25%;
            background: #fddbc2;
        }

        .c2 {
            width: 10%;
            background: #dbdee2;
        }

        .k2 {
            width: 20%;
            background: #fddbc2;
        }

        .c3 {
            width: 10%;
            background: #dbdee2;
        }

        .k3 {
            width: 10%;
            background: #fddbc2;
        }

        .c4 {
            width: 10%;
            background: #dbdee2;
        }

        .c5 {
            width: 45%;
            background: #dbdee2;
        }

        .main-wrapper {
            display: block;
            height: 100%;
            width: 100%;
        }

        .container {
            display: block;
            height: 100%;
            width: 100%;
        }

        .left-content {
            width: 265px;
            height: 100%;
            float: left;
            display: block;
            border-collapse: collapse;
        }

        .left_div {
            width: 100%;
            border-right: 1px solid #ccc;
        }

        .left-content .search {
            height: 34px;
            padding: 2px 4px;
            border-bottom: 1px solid #aeaeae;
        }

        .left-content .navlink {
            height: calc(100% - 40px);
        }

        .left-content .search .search-div {
            width: 100%;
            height: 100%;
            float: left;
            display: block;
            align-items: center;
        }

        .search-input-wrapper {
            width: 200px;
            height: 25px;
            line-height: 25px;
            display: inline-block;
            float: left;
        }

        .left-content .search-input {
            width: 200px;
            height: 26px;
            overflow: scroll;
            vertical-align: middle;
            margin: 4px 0;
        }

        .search-button-wrapper {
            display: inline-block;
            height: 25px;
            line-height: 25px;
            vertical-align: middle;
            float: left;
        }

        .left-content .search-button {
            width: 50px;
            height: 26px;
            margin: 4px 0 4px 6px;
        }

        .right-content {
            height: 100%;
            width: calc(100% - 265px);
            display: block;
            float: left;
            overflow: auto;
        }

        .right-content .right_div {
            min-width: 1200px;
        }

        .right-content .title {
            height: 38px;
            width: 100%;
            overflow: hidden;
            text-align: center;
            margin: 0 auto;
            vertical-align: middle;
            border-bottom: 1px solid #aeaeae;
            box-shadow: 0 .425rem .725rem rgba(0, 0, 0, .275) !important;
        }

        .right-content .title span {
            display: inline-block;
            height: 38px;
            line-height: 38px;
            width: 100%;
            font-weight: bold;
        }

        .right-content .tables {
            padding: 5px 5px 20px;
            overflow: auto;
            height: calc(100% - 40px);
            box-sizing: border-box;
        }

        .right-content .tables table {
            margin-bottom: 5px;
        }

        .right-content .tables table tr:first-child {
            font-weight: bold;
            background: #ced8f1;
        }

        .navlink .table-name {
            color: #0d6efd;
            text-decoration: none;
            font-weight: bold;
        }

        .navlink .table-block {
            border: 1px dashed #DFE1E5;
            background: #F6F8FA;

        }

        .navlink .table-block:hover {
            border: 1px dashed #aeb7c9;
            background: #e7f3ff;
        }

        .navlink .table-comment {
            display: block;
            color: #616568;
            font-size: 10px;
        }

        .right-content .tables::-webkit-scrollbar {
            width: 10px;
        }

        .navlink > ul::-webkit-scrollbar {
            width: 10px;
        }

        table {
            font-size: 14px;
            color: #444;
            border-spacing: 0;
            border: 1px solid grey;
            border-collapse: separate;
        }

        table.table-content {
            /*min-width: 980px;*/
        }

        table.table-content.table-keyinfo {

        }

        th, tr, td {
            padding: 4px;
        }

        table.table-keyinfo {
            margin-bottom: 15px !important;
        }

        table.table-keyinfo tr:first-child td, table.table-keyinfo tr:first-child td p {
            background: #dbdee2;
        }

        ul {
            height: 100%;
            overflow-y: scroll;
            padding-left: 5px;
            padding-top: 0;
            margin-top: 0;
        }

        ul li {
            list-style: none;
        }

        ::-webkit-scrollbar-button {
            display: none;
        }

        ::-webkit-scrollbar-thumb {
            background-color: #e1e1e1;
            border-radius: 4px;
            transition: 0.3s ease-in-out;
        }

        ::-webkit-scrollbar-thumb:hover {
            background-color: #c1c1c1;
        }

        ::-webkit-scrollbar-track {
            background-color: initial;
        }

        .color-red {
            color: red;
        }

    </style>
</head>
<body>

<div class="main-wrapper">
    <div class="container">
        <div class="left-content">
            <div class="left_div">
                <div class="search">
                    <div class="search-div">
						<span class="search-input-wrapper">
	                        <input id="search-input" class="search-input" type="text" placeholder="输入关键字进行搜索"/>
						</span>
                        <span class="search-button-wrapper">
	                        <input id="search-button" class="search-button" type="button" value="搜索"/>
						</span>
                    </div>
                </div>
                <div id="navlink" class="navlink">
                    <ul>
                        <#if tables?? && tables?size gt 0>
                            <#list tables as table>
                                <li>
                                    <p class="normal table-block">
                                        <a class="table-name" href="javascript:scrollTo('${table.tableName}');">${table.tableName}</a>
                                        <span class="table-comment">
                                            <#if ((table.tableComment)!"")?trim?length gt 0>
                                                ${table.tableComment}
                                            <#else>
                                                --
                                            </#if>
                                        </span>
                                    </p>
                                </li>
                            </#list>
                        </#if>
                    </ul>
                </div>
            </div>
        </div>
        <div class="right-content">
            <div class="right_div">
                <div id="title" class="title">
                    <span>${database}-表结构列表</span>
                </div>
                <div id="tables" class="tables">
                    <#if tables?? && tables?size gt 0>
                        <#list tables as table>
                            <table id="${table.tableName}" class="table-content" width="100%" cellspacing="0" cellapdding="2" border="1">
                                <tr>
                                    <td colspan="5">
                                        ${table.tableName}
                                        <#if ((table.tableComment)!"")?trim?length gt 0>
                                            (${table.tableComment})
                                        <#else>
                                            (--)
                                        </#if>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" class="fieldcolumn c1">名称</td>
                                    <td align="center" valign="top" class="fieldcolumn c2">类型</td>
                                    <td align="center" valign="top" class="fieldcolumn c3">是否可为空</td>
                                    <td align="center" valign="top" class="fieldcolumn c4">默认值</td>
                                    <td align="center" valign="top" class="fieldcolumn c5">注释</td>
                                </tr>
                                <#if table.columns?? && table.columns?size gt 0>
                                    <#list table.columns as column>
                                        <tr>
                                            <td align="left" valign="top"><p class="normal">${column.columnName}</td>
                                            <td align="left" valign="top"><p class="normal">${column.dataType}</td>
                                            <td align="left" valign="top"><p class="normal">${column.nullable}</td>
                                            <td align="left" valign="top"><p class="normal">${column.dataDefault!}</td>
                                            <td align="left" valign="top"><p class="normal">${column.columnComments!}</td>
                                        </tr>
                                    </#list>
                                </#if>
                            </table>

                            <table class="table-keyinfo" width="55%" cellspacing="0" cellapdding="2" border="1">
                                <tr>
                                    <td class="k1" align="left" valign="top"><p class="fieldcolumn key1">索引</p></td>
                                    <td class="k2" align="left" valign="top"><p class="fieldcolumn key2">包含列</p></td>
                                    <td class="k3" align="left" valign="top"><p class="fieldcolumn key3">索引类型</p></td>
                                </tr>
                                <#if table.keys?? && table.keys?size gt 0>
                                    <#list table.keys as key>
                                        <tr>
                                            <#if key.index?upper_case=="PRIMARY">
                                                <td align="left" valign="top"><p class="normal color-red">${key.index}</td>
                                            <#else>
                                                <td align="left" valign="top"><p class="normal">${key.index}</td>
                                            </#if>

                                            <td align="left" valign="top"><p class="normal">${key.columns}</td>
                                            <td align="left" valign="top"><p class="normal">${key.type}</td>
                                        </tr>
                                    </#list>
                                </#if>
                            </table>
                        </#list>
                    </#if>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    const search_input = document.querySelector("#search-input");
    const search_btn = document.querySelector("#search-button");
    const lis = document.querySelectorAll('li');
    search_btn.onclick = function () {
        const input_str = search_input.value;
        for (let i = 0; i < lis.length; i++) {
            const table_name = lis[i].querySelector('p>a.table-name').innerText.trim().toUpperCase();
            const table_conmment = lis[i].querySelector('p>span.table-comment').innerText.trim().toUpperCase();
            const searchString = input_str.toUpperCase();
            if (table_name.indexOf(searchString) >= 0 || table_conmment.indexOf(searchString) >= 0) {
                lis[i].style.display = '';
            } else {
                lis[i].style.display = 'none';
            }
        }
    }

    window.onload = function () {
//        const clientHeight = document.body.clientHeight;
//        const headTitle = document.getElementById("title");
//        const navlink = document.getElementById("navlink");
//        const tables = document.getElementById("tables");
//        tables.style.height = (clientHeight - headTitle.offsetHeight) + "px";
//        navlink.style.height = (clientHeight - headTitle.offsetHeight) + "px";
//        tables.style.overflow = "auto";
//        navlink.style.overflow = "auto";

        document.onkeydown = function (ev) {
            const event = ev || window.event;
            const key = event.which || event.keyCode || event.charCode;
            if (key == 13) {
                const search_input = document.querySelector("#search-input");
                const search_btn = document.querySelector("#search-button");
                if (search_input == document.activeElement) {
                    search_btn.click();
                }
            }
        }

        const search_input = document.querySelector("#search-input");
        search_input.onkeyup = function () {
            const input_str = search_input.value;
            if (!input_str) {
                const lis = document.querySelectorAll('li');
                for (let i = 0; i < lis.length; i++) {
                    lis[i].style.display = '';
                }
            }
        }

    }

    function scrollTo(id) {
        const elem = document.getElementById(id);
        const tables = document.getElementById("tables");

        tables.scrollTop = elem.offsetTop - 40;
    }

</script>
</body>
</html>