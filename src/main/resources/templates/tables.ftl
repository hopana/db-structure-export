<html>
<head>
    <title>${database}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <style type="text/css">
        body {
            font-family: "微软雅黑", Verdana, Arial, Helvetica, sans-serif;
        }

        .toptext {
            color: #000000;
            font-size: 20px;
            font-weight: 600;
            width: 550px;
            background-color: #999999;
        }

        .normal {
            font-size: 11px;
            font-weight: normal;
            color: #000000;
            overflow-wrap: break-word;
        }

        .fieldheader {
            color: #000000;
            font-size: 12px;
            font-weight: 600;
            background-color: #c0c0c0;
        }

        .fieldcolumn {
            color: #000000;
            font-size: 10px;
            font-weight: 600;
            background-color: #ffffff;
        }

        .c1 {
            width: 330px;
        }

        .c2 {
            width: 150px;
        }

        .c3 {
            width: 100px;
        }

        .c4 {
            width: 100px;
        }

        .c5 {
            width: 100px;
        }

        .c6 {
            width: 500px;
        }

        .header {
            background-color: #ECE9D8;
        }

        .headtext {
            color: #000000;
            font-size: 12;
            font-weight: 600;
            width: 550px;
            background-color: #c0c0c0;
        }

        .navdiv {
            width: 100%;
            height: 400px;
            overflow-y: scroll;
            border: 1px solid #aaa;
        }

        br.page {
            page-break-after: always
        }

        table.table-content {
            font-size: 14px;
            color: #444;
            border-spacing: 0;
            border: 1px #grey;
            border-collapse: separate;
        }

        table.table-content th,tr,td {
            padding:4px;
        }

        .navdiv::-webkit-scrollbar {
            width: 6px;
        }

        ::-webkit-scrollbar-button {
            display: none;
        }

        ::-webkit-scrollbar-thumb {
            background-color: #e1e1e1;
            border-radius: 4px;
        }

        ::-webkit-scrollbar-thumb:hover {
            background-color: #c1c1c1;
        }

        ::-webkit-scrollbar-track {
            background-color: initial;
        }

        #search-input {
            width:300px;
            height: 26px;
            border: 1px solid #e1e1e1;
            border-radius: 2px;
            margin-bottom: 5px;
            font-size:12px;
        }
    </style>
</head>
<body bgcolor='#ffffff' topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="toptext"><p align="center">${database}</td>
    </tr>
</table>
<a name="header">&nbsp</a>

<div>
    <input type="input" id="search-input" placeholder="搜索" />
</div>
<div class="navdiv">
    <ul>
        <#if tables?? && tables?size gt 0>
            <#list tables as table>
                <li>
                    <a href="#${table.tableName}">
                        <p class="normal">
                        ${table.tableName}
                        <#if ((table.tableComment)!"")?trim?length gt 0>
                            (${table.tableComment})
                        <#else>
                            (--)
                        </#if>
                    </a>
                </li>
            </#list>
        </#if>
    </ul>
</div>

<br class=page>

<#if tables?? && tables?size gt 0>
    <#list tables as table>
        <p><a name='${table.tableName}'>&nbsp</a>
        <table class="table-title" width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td class="headtext" width="30%" align="left" valign="top">
                    ${table.tableName}
                    <#if ((table.tableComment)!"")?trim?length gt 0>
                        (${table.tableComment})
                    <#else>
                        (--)
                    </#if>
                </td>
                <td>&nbsp</td>
            <tr>
        </table>

        <table class="table-content" width="100%" cellspacing="0" cellapdding="2" border="1">
            <tr>
                <td align="center" valign="top" class="fieldcolumn c1">名称</td>
                <td align="center" valign="top" class="fieldcolumn c2">类型</td>
                <td align="center" valign="top" class="fieldcolumn c3">是否可为空</td>
                <td align="center" valign="top" class="fieldcolumn c4">默认值</td>
                <#--<td align="center" valign="top" class="fieldcolumn c5">存储</td>-->
                <td align="center" valign="top" class="fieldcolumn c6">注释</td>
            </tr>
            <#if table.columns?? && table.columns?size gt 0>
                <#list table.columns as column>
                    <tr>
                        <td align="left" valign="top"><p class="normal">${column.columnName}</td>
                        <td align="left" valign="top"><p class="normal">${column.dataType}</td>
                        <td align="left" valign="top"><p class="normal">${column.nullable}</td>
                        <td align="left" valign="top"><p class="normal">${column.dataDefault!}</td>
                        <#--<td align="left" valign="top"><p class="normal">--</td>-->
                        <td align="left" valign="top"><p class="normal">${column.columnComments!}</td>
                    </tr>
                </#list>
            </#if>
        </table>
        <!-- TODO:索引 -->
        <a href="#header"><p class="normal">返回</a><br class=page>
    </#list>
</#if>

<h1 width="100%"/>

<script>
    var input = document.querySelector('#search-input');
    var lis = document.querySelectorAll('li');
    input.oninput=function(){
        var input_str=input.value;
        for(var i=0; i<lis.length; i++){
            var a = lis[i].querySelector('p>a');
            if(a.innerText.toUpperCase().indexOf(input_str.toUpperCase()) >= 0){
                lis[i].style.display='';
            } else {
                lis[i].style.display='none';
            }
        }
    }

</script>
</body>
</html>