package com.hp.tools.db;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.hp.tools.model.Column;
import com.hp.tools.model.Key;
import com.hp.tools.model.KeyInfo;
import com.hp.tools.model.Table;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

/**
 * 获取数据库连接
 *
 * @author hupan
 */
public abstract class DB {

    private static DataSource ds;

    protected static String db;

    /**
     * 数据库驱动
     */
    protected static String driverName;
    /**
     * 数据库连接url
     */
    protected static String url;
    /**
     * 用户名
     */
    protected static String username;
    /**
     * 密码
     */
    protected static String password;

    protected static Properties properties;

    public static void init(Properties properties) throws Exception {
        ds = DruidDataSourceFactory.createDataSource(properties);
        db = properties.getProperty("db");
        driverName = properties.getProperty("driverClassName");
        url = properties.getProperty("url");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
    }

    public DB() {
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public static DataSource getDataSource(){
        return ds;
    }

    protected String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * 执行sql
     */
    public abstract ResultSet executeQuery(String sql);

    protected void closeAll(Connection conn, Statement stmt, ResultSet rs) {
        try {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取数据库中所有表的表名，并添加到列表结构中
     *
     * @return 表名集合
     * @throws SQLException
     */
    public abstract List<String> getTableNameList();

    /**
     * 从数据库中获取指定条件的表名
     *
     * @return
     */
    public abstract List<Table> getAllTables();

    /**
     * 获取数据表中所有列的列名，并添加到列表结构中
     *
     * @param tableName 表名
     * @return 列名集合
     * @throws SQLException
     */
    public abstract List<String> getColumnNameList(String tableName);


    /**
     * 根据表名获取表结构信息
     *
     * @param tableName
     * @return
     */
    public abstract List<Column> getStructOfTable(String tableName);


    /**
     * 根据表名获取索引信息
     *
     * @param tableName
     * @return
     */
    public abstract List<KeyInfo> getIndex(String tableName) throws Exception;

}