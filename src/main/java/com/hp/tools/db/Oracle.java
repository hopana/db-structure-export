package com.hp.tools.db;

import com.hp.tools.model.Column;
import com.hp.tools.model.Key;
import com.hp.tools.model.KeyInfo;
import com.hp.tools.model.Table;

import java.sql.*;
import java.util.List;

/**
 * 获取数据库连接
 *
 * @author hupan
 */
public class Oracle extends DB {

    @Override
    public ResultSet executeQuery(String sql) {
        return null;
    }

    @Override
    public List<String> getTableNameList() {
        return null;
    }

    @Override
    public List<Table> getAllTables() {
        return null;
    }

    @Override
    public List<String> getColumnNameList(String tableName) {
        return null;
    }

    @Override
    public List<Column> getStructOfTable(String tableName) {
        return null;
    }

    @Override
    public List<KeyInfo> getIndex(String tableName) throws Exception {
        return null;
    }
}