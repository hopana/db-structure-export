package com.hp.tools.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 索引
 */
@Getter
@Setter
@ToString
public class Key {
	private String table;
	private Integer nonUnique;
	private String keyName;
	private Integer seqInIndex;
	private String columnName;
	private String collation;
	private Integer cardinality;
	private String subPart;
	private String packed;
	private String nullable;
	private String indexType;
	private String comment;
	private String indexComment;
}
