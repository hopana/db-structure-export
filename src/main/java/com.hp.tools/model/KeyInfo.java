package com.hp.tools.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class KeyInfo {

	private String index;
	private String columns;
	private String type;

	public static KeyInfo ofKey(Key key) {
		KeyInfo keyInfo = new KeyInfo();
		keyInfo.setIndex(key.getKeyName());
		keyInfo.setColumns(key.getColumnName());
		keyInfo.setType("");

		if (key.getNonUnique() == 0) {
			if ("PRIMARY".equalsIgnoreCase(key.getKeyName())) {
				keyInfo.setType("主键索引");
			} else {
				keyInfo.setType("唯一索引");
			}
		}

		return keyInfo;
	}

}
