package com.hp.tools.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class Table {

    /**
     * 表名
     */
    private String tableName;
    /**
     * 表注释
     */
    private String tableComment;

    /**
     * 列集合
     */
    private List<Column> columns;

    /**
     * 索引
     */
    private List<KeyInfo> keys;

}
