package com.hp.tools;

import com.hp.tools.db.DB;
import com.hp.tools.model.Table;
import com.hp.tools.task.TaskExecutor;
import com.hp.tools.db.Mysql;
import com.hp.tools.utils.FreeMarkerUtils;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 数据库结果导出
 *
 * @author hupan
 * @since 2018-06-01 10:21
 */
public class Exporter {
	public static void main(String[] args) {
		try (// 使用ClassLoader加载properties配置文件生成对应的输入流
		     InputStream is = Exporter.class.getClassLoader().getResourceAsStream("druid.properties");
		     // 使用properties对象加载输入流
		     InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8)
		) {
			Properties properties = new Properties();
			properties.load(isr);
			DB.init(properties);

			List<Table> tables = Collections.synchronizedList(Mysql.getInstance().getAllTables());
			new TaskExecutor().invokeTask(tables);

			String db = properties.getProperty("db");

			Map<String, Object> dataMap = new HashMap<>(32);
			dataMap.put("database", db);
			dataMap.put("tables", tables);

			FreeMarkerUtils.process("tables-template.ftl", "D:\\" + db + ".html", dataMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
