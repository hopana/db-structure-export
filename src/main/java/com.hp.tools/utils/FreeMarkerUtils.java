package com.hp.tools.utils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Map;

/**
 * FreeMarker文件生成工具类
 *
 * @author hupan
 * @since 2017-03-20 17:00
 */
public class FreeMarkerUtils {

    private FreeMarkerUtils() {

    }

    private static final String TEMPLATE_PATH = "templates";

    /**
     * 获取Template对象
     *
     * @param templateName 模版名称（不带路径）
     * @return Template对象
     * @throws IOException 抛出IOException
     */
    public static Template getTemplate(String templateName) throws IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_30);
        cfg.setDefaultEncoding(StandardCharsets.UTF_8.name());
        URL url = FreeMarkerUtils.class.getClassLoader().getResource(TEMPLATE_PATH + "/" + templateName);
        if (url == null) {
            throw new IOException("模板文件不存在");
        }

        cfg.setDirectoryForTemplateLoading(new File(url.getPath()).getParentFile());

        return cfg.getTemplate(templateName);
    }

    public static void process(String templateName, String fileName, Map<String, Object> dataModel) throws IOException, TemplateException {
        Template template = getTemplate(templateName);

        File file = new File(fileName);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8);
        template.process(dataModel, writer);
        writer.flush();
        writer.close();
    }

}
